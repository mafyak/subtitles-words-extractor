import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class SubtitleReader {

  public void readAll() {
    URL resource = this.getClass().getClassLoader().getResource("subtitles.srt");
    try {
      Set<String> result = new HashSet<>();
      File file = new File(resource.toURI());
      List<String> strings = Files.readAllLines(file.toPath(), StandardCharsets.UTF_8);
      for (String string : strings) {
        String[] words = string.split(" ");
        for (String word : words) {
          if (word.matches("[a-zA-Z']+")) {
            result.add(word);
          }
        }
      }
      List<String> orderedList = result.stream()
          .filter(word -> word.length() > 3)
          .map(String::toLowerCase)
          .sorted()
          .collect(Collectors.toList());

      URL writeResource = this.getClass().getClassLoader().getResource("result.txt");
      File resultFile = new File(writeResource.toURI());

      BufferedWriter writer = new BufferedWriter(new FileWriter(resultFile));
      for (String word : orderedList) {

        writer.append(word);
        writer.newLine();
      }
      writer.flush();
      writer.close();
    } catch (URISyntaxException | IOException e) {
      System.out.println();
    }
  }
}
